class Q :
    def __init__ (self,content):
        self._hiddenlist = list(content)
    def push(self,value):
        self._hiddenlist.insert(0,value)
    def pop (self):
        return self._hiddenlist.pop(-1)
    def __repr__(self):
        return 'Q({})'.format(self._hiddenlist)

q=Q([1,2,3])
print(q)
q.push(0)
print(q)
q.pop()
print(q)
print(q._hiddenlist)

